# editorconfig-templates

Common starting points for [`.editorconfig`](https://editorconfig.org) files.

## Usage

A simple cloud function exists to assist in downloading a particular configuration from this repository:

`curl https://EXAMPLE.com/functions/endpoint`

Alternatively, just copy and paste the contents of any of the `.editorconfig` files in [`lib`](/lib/).

---

<sub>Emoji used for repository logo designed by <a href="https://openmoji.org/">OpenMoji</a> – the open-source emoji and icon project. License: CC BY-SA 4.0</sub>

---
